# NCTU

## Venue

*   Internaional Conference Room (230 ppl)
    *   Jul 25 ~ Aug 6
    *   08:00 ~ 20:00
*   Conference Room 1 (100 ppl)
    *   Jul 25 ~ Aug 6
    *   08:00 ~ 20:00
*   Conference Room 4 (150 ppl)
    *   Jul 25 ~ Aug 6
    *   08:00 ~ 20:00
*   MIRC-NCTU 1/2 floor
    *   Jul 25 ~ Aug 6
    *   08:00 ~ 19:00
*   ES201 (60 ppl, quiet hacklab)
    *   Jul 21 ~ Aug 6
    *   00:00 ~ 24:00
*   ES203 (33 ppl, hacklab)
    *   Jul 21 ~ Aug 6
    *   00:00 ~ 24:00
*   VIP meeting rom (9 ppl, childcare)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 20:00
*   ES510 (12 ppl, video)
    *   Jul 21 ~ Aug 6
    *   00:00 ~ 24:00
*   ES815 (18 ppl, FD/orga)
    *   Jul 21 ~ Aug 6
    *   00:00 ~ 24:00
*   ES204 (might be superseded)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 12:00, 13:00 ~ 17:00
*   ES705 (7坪)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 17:00, locked in other time. (request to change to 24h for NOC)
*   ES724 (10坪)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 17:00, locked in other time. (request to change to 24h for NOC)
*   ES701-R1 (3,4坪)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 17:00, locked in other time.
*   ES701-R8 (3,4坪)
    *   Jul 21 ~ Aug 6
    *   08:00 ~ 17:00, locked in other time.

## Other Requirements

*   Refrigerator
    *   MIRC-NCTU 803 has small/medium refrigerator.
*   Ice Maker
    *   體育館和綜合球館有製冰機
*   Night Market
    *   據悉僅學校三大活動(校慶、運動會和梅竹)才能舉辦夜市擺攤，若您有其他想法，可以直接跟曾先生連繫(分機50905)。
*   辦桌
    *   基本上，一樓、二樓開放空間可以擺桌，我們常辦Buffet (外燴)
        *   一樓20桌出頭 (200 ppl)
        *   第一會議室 (30 ppl)
        *   二樓40桌出頭 (400 ppl, 無冷氣)
    *   辦桌要現場煮，室內不可以生火，學校有在外面空間(體育館停車場)辦過
    *   若要借用體育館前面的空地(一般沒有外借)，需遞送活動計畫書(說明多少桌等資訊)給體育室的主管，再由主管決定是否同意借用。若同意借用，有兩點提醒
        *   要負責後續清潔，
        *   關於搭棚方式由於不能鑽地，因此僅能以沙袋或水桶固定。
*   校園宣傳
    *   可提供海報請系辦工讀生協助張貼。

## Physical Address for Shipment

*   We will start to receive shipment from Jul 1.

```
收件人：國立交通大學 電子資訊研究大樓 李安蕙
地址：30010新竹市東區大學路1001號
電話：03-5712121*31944
```

```
AnHui Lee, Microelectronics and Information Research Center, National Chiao Tung University
No.1001, University Rd., East Dist., Hsinchu City 300, Taiwan (R.O.C.)
+886-3-5712121*31944
```
